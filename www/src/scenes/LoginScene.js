/**
 * Created by zhanghua on 2018/5/25.
 * 登录场景
 */
var scenes = scenes || {};
var flax = flax || {};
scenes.loginScene = flax.Scene.extend({
    onEnter: function () {
        this._super();
        var item = flax.createDisplay(res.login, "a5");
        this.addChild(item);

        //标题内容
        item['scoreTxt'].text = item['scoreTxt'].text.replace('{n}', flax.userData.maxScore);
        item['conTxt'].text = item['conTxt'].text.replace('{n}', config.winRate * 100 + "%");

        //设置按钮状态
        //继续游戏
        var _preLevel = flax.userData.get('userLevel');
        var _state = _preLevel === 0 ? ButtonState.DISABLED : ButtonState.SELECTED;
        item["viewGroup"].setState(_state);
        //无尽模式
        _state = flax.userData.gameOver ? ButtonState.SELECTED : ButtonState.DISABLED;
        item["viewFriends"].setState(_state);
        //设置按钮事件
        //开始游戏按钮
        flax.addListener(item["startBtn"], function () {
            flax.userData.userLevel = 0;
            flax.replaceScene("main");
        }, InputType.click);
        //继续游戏
        flax.addListener(item["viewGroup"], function (target, event) {
            var _tar = target.currentTarget;
            if (_tar.getState() === ButtonState.DISABLED)
                return;
            //读取当前关卡,这里是防止当前关卡在无限关卡
            flax.userData.userLevel = flax.userData.get('userLevel');
            hexHelper.totalScore = flax.userData.userPoint;
            flax.replaceScene("main");
            // 这是原来接微信的邀请好友
            // var key = allConst.weixinKey;
            // var isFromRankLittle = false;
            // var isGroup = false;
            // var rank = new rankList(key,isFromRankLittle,isGroup);
            // self.addChild(rank);
        }, InputType.click);
        //无尽模式
        flax.addListener(item["viewFriends"], function (target, event) {
            var _tar = target.currentTarget;
            if (_tar.getState() === ButtonState.DISABLED)
                return;
            flax.userData.userLevel = -1;
            flax.replaceScene("main");
            //   这是原来排行榜按钮，微信小游戏使用
            //  var key = allConst.weixinKey;
            //  var isGroup = true;
            //  var isFromRankLittle = false;
            //  weixinHelper.mainScene = self;
            // weixinHelper.shareGroup(key,isFromRankLittle,isGroup);
        }, InputType.click);
    },
    onExit: function () {
        this._super();
    }
});
