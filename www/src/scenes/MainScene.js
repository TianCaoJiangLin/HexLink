/**
 * Created by zhanghua on 2018/5/14.
 * 游戏主界面
 */
var scenes = scenes || {};
var flax = flax || {};
scenes.mainScene = flax.Scene.extend({
    onEnter: function () {
        this._super();
        
        //下面是生成六角形的代码
        var board = new hexBoard();
        this.addChild(board);
        var body = new hexBody();
        this.addChild(body);

        //下面是生成标题的代码
        var info = new infoBoard();
        this.addChild(info);

        //设置主屏用来显示最后的win面板
        hexHelper.mainScene = this;
    },
    onExit: function () {
        this._super();
    }
});
