/**
 * Created by Administrator on 2018/5/22.
 * 这里主要处理主域和微信相关的操作
 */
var weixinHelper = {
    isOfWeiXin: (typeof wx !== "undefined"), OfWeiXin: false,   //是否在微信环境运行
    // sprite: null,    //用来显示共享屏
    openDataContext: null,   //开放域
    sharedCanvas: null,      //共享屏
    //初始化相关数据
    initData: function () {
        if (!this.isOfWeiXin)
            return;
        this.openDataContext = wx.getOpenDataContext();
        this.sharedCanvas = this.openDataContext.canvas;
        this.sharedCanvas.width = allConst.width;
        this.sharedCanvas.height = allConst.height;
    },
    //设置微信相关内容
    initWeiXin: function () {
        var self = this;
        if (this.isOfWeiXin) {
            this.login(self.getUserInfo);
            //初始化拖管数据
            this.initScore('score', 0);
            //显示右上角转发菜单
            this.showShareMenu();
        }
    },
    //登录
    login: function (callback) {
        wx.login({
            success: function () {
                //todo 获得用户session key
                if (callback)
                    callback();
            }
        });
    },
    //获得用户授权，第一次会弹出窗口确认，无论授权与否，均可从子域拿到用户头像url，用来排行榜时判断是否本用户
    getUserInfo: function () {
        //在回调里self = this 没用，因为此时this已经不是本对象了
        //主要是弹用户授权
        wx.getUserInfo({
            fail: function (res) {
                // iOS 和 Android 对于拒绝授权的回调 errMsg 没有统一，需要做一下兼容处理
                if (res.errMsg.indexOf('auth deny') > -1 || res.errMsg.indexOf('auth denied') > -1) {
                    // 处理用户拒绝授权的情况,此时需要重新引导
                    console.log("用户拒绝授权");
                    // //再次尝试会失败，此时子域可以拿到url
                }
            },
            success: function (res) {
                //todo 进一步获得用户信息
                console.log("获得用户信息成功");
            },
            //从子域拿用户头像url
            complete: function () {
                weixinHelper.openDataContext.postMessage({
                    text: 'getUserInfo'
                });
            }
        });
    },
    // getUserInfo: function () {
    //     //todo 出现一遮挡面板和关闭按钮
    //     var button = wx.createUserInfoButton({
    //         type: 'text',
    //         text: '点击授权用户信息',
    //         style: {
    //             left: 60,
    //             top: 214,
    //             width: 200,
    //             height: 40,
    //             lineHeight: 40,
    //             backgroundColor: '#226699',
    //             color: '#663388',
    //             textAlign: 'center',
    //             fontSize: 16,
    //             borderRadius: 4
    //         }
    //     });
    //     button.onTap(function(res)  {
    //         //todo 可以获得iv entradata等
    //         console.log(res)
    //         button.hide();
    //         button.destroy();
    //     });
    // },
    //显示右上角转发菜单
    showShareMenu: function () {
        wx.showShareMenu({
            withShareTicket: true
        });
    },
    //游戏在第一次运行时需要初始化分数，在子域进行
    initScore: function (key, value) {
        this.openDataContext.postMessage({
            text: 'initScore',
            key: key,
            value: value
        });
    },
    //从好友排行榜里显示群排行榜
    showGroupRank: function (ticket, key) {
        this.openDataContext.postMessage({
            text: 'getGroupCloudStorage',
            ticket: ticket,
            key: key
        });
        if (rankItem != null && rankItem['rankTitle'])
            rankItem['rankTitle'].text = "群排行榜";
    },
    //显示群排行榜
    shareGroup: function (key ,isFromRankLittle,isGroup) {
        var self = this;
        wx.shareAppMessage({
            success: function (res) {
                //发到一个群里
                if(res.shareTickets){
                    var ticket = res.shareTickets[0];
                    if(isGroup){
                        var newRank = new rankList(key ,isFromRankLittle,isGroup,ticket);
                        weixinHelper.mainScene.addChild(newRank);
                        if (rankItem != null && rankItem['rankTitle'])
                            rankItem['rankTitle'].text = "群排行榜";
                    }else{
                        self.showGroupRank(ticket,key);
                    }
                }else{
                    //todo 发给好友
                }
                //todo 进一步的解密，先不管
                // wx.getShareInfo({
                //     shareTicket:res.shareTickets,
                //     success:function(res){
                //     }
                // });
            },
            fail: function (res) {
                //todo
            }
        });
    }
}

