/**
 * Created by Administrator on 2018/5/28.
 */
var flax = flax || {};
//显示好友或群排行榜，默认显示好友
//需要PIXI库，完全运行需要微信运行环境
var rankItem = null;  //暴露给外部的对象接口，更改标题或者下面按钮内容时使用
var rankList = flax.Container.extend({
    //可以在子域通过url验证是不是本人，此时如果没授权仍然拿不到openId
    key:"",     //查询托管数据的key
    sprite:null,  //用来显示共享屏
    isFromRankLittle:false,                 //是否从小排行榜调用
    isGroup:false,
    ticket:"",                              //如果是群排行榜，shareTicket
    //构造器
    ctor: function(key,isFromRankLittle,isGroup,ticket) {
        this._super();
        this.key = key;
        this.isFromRankLittle = isFromRankLittle;
        this.isGroup = isGroup;
        this.ticket = ticket;
    },
    onEnter: function () {
        this._super();
        this.initView();
        this.scheduleUpdate();

    },
    onExit: function () {
        this._super();
        this.key = null;
        this.sprite = null;
        this.ticket = null;
    },
    initView: function () {
        var item = flax.createDisplay(res.rank, "a38");   //资源库
        this.addChild(item);
        rankItem = item;  //暴露给外部的对象接口，更改标题或者下面按钮内容时使用
        if (!weixinHelper.isOfWeiXin)
            return;
        this.hide(item);
        this.setScript(item);
        this.calCoor(item);
    },
    //隐藏原始元素
    hide:function(item){
        item['rank'].visible = false;
        item['icon'].visible = false;
        item['nickName'].visible = false;
        item['score'].visible = false;
        item['rankUser'].visible = false;
        item['iconUser'].visible = false;
        item['nickNameUser'].visible = false;
        item['scoreUser'].visible = false;
        item['backItem'].visible = false;
    },
    //设置监听器
    setScript: function (item) {
        var self = this;
        //退出按钮
        flax.addListener(item['exitBtn'], function () {
            rankItem = null;
            self.destroy();
            //退出排行榜，通知子域排行榜清空,区分条件是否要重绘
            var mes = {
                text: 'exitRank',
                refresh:false  //不需要重绘
            };
            //如果从小排行榜调用再关闭，需要重绘
            if(self.isFromRankLittle)
                mes.refresh = true;
            weixinHelper.openDataContext.postMessage(mes)
        }, InputType.click);
        flax.addListener(item['preBtn'], function () {
            self.prePage();
        }, InputType.click);
        flax.addListener(item['nextBtn'], function () {
            self.nextPage();
        }, InputType.click);
        flax.addListener(item['groupBtn'], function () {
            weixinHelper.shareGroup(self.key,false,false);
        }, InputType.click);
    },
    //微信环境下计算坐标
    calCoor: function (item) {
        //背景条框的宽和高
        var itemHeight = item["backItem"].height;
        var itemWidth = item["backItem"].width;
        //名次//ICON //nickName //score位置
        var rank = item["rank"].position.x + "-" + item["rank"].position.y;
        var icon = item["icon"].position.x + "-" + item["icon"].position.y;
        var nickName = item["nickName"].position.x + "-" + item["nickName"].position.y;
        var scorePos = item["score"].position.x + "-" + item["score"].position.y;
        //排行榜最下方显示自己的元素位置
        var rankUser = item["rankUser"].position.x + "-" + item["rankUser"].position.y;
        var iconUser = item["iconUser"].position.x + "-" + item["iconUser"].position.y;
        var nickNameUser = item["nickNameUser"].position.x + "-" + item["nickNameUser"].position.y;
        var scorePosUser = item["scoreUser"].position.x + "-" + item["scoreUser"].position.y;
        var backItem = item['backItem'].position.x + "-" + item["backItem"].position.y;
        //传送的消息
        var message = {
            text: 'getFriendCloudStorage',
            rank: rank,
            icon: icon,
            nickName: nickName,
            scorePos: scorePos,
            rankUser: rankUser,
            iconUser:iconUser,
            nickNameUser:nickNameUser,
            scorePosUser:scorePosUser,
            backItem:backItem,
            itemHeight:itemHeight,
            itemWidth:itemWidth,
            key:this.key
        };
        //区分从小排行榜调用
        if(this.isFromRankLittle){
            message.text = 'showAllRank';
        }else if (this.isGroup){
            message.text = 'getGroupCloudStorage';
            message.ticket = this.ticket;
        }
        this.showCanvas(message);
    },
    //发送消息来显示共享屏
    showCanvas:function(message){
        weixinHelper.openDataContext.postMessage(message);
        //显示共享屏
        this.sprite = new PIXI.Sprite();
        this.addChild(this.sprite);
        var btexture = new PIXI.BaseTexture(weixinHelper.sharedCanvas);
        this.sprite.texture = new PIXI.Texture(btexture);
    },
    update:function(){
        var btexture = new PIXI.BaseTexture(weixinHelper.sharedCanvas);
        this.sprite.texture = new PIXI.Texture(btexture);
    },
    //下一页
    nextPage: function () {
        weixinHelper.openDataContext.postMessage({
            text: 'nextPage'
        });
    },
    //上一页
    prePage: function () {
        weixinHelper.openDataContext.postMessage({
            text: 'prePage'
        });
    },
    //分享给好友，暂时没用，weixinHelp.shareGroup
    inviteFriend: function () {
        wx.shareAppMessage({
            success: function (res) {
                //todo
            },
            fail: function (res) {
                //todo
            }
        });
    }
});

//游戏结束时显示小排行榜，此时多了一个功能，要设置游戏的最高分
//主要结构和上面的rankList一样
var rankListLittle = flax.Container.extend({
    key:"",
    score:0,
    sprite:null,
    ctor: function(key,score) {
        this._super();
        this.key = key;
        this.score = score;
    },
    onEnter: function () {
        this._super();
        this.initView();
        this.scheduleUpdate();
    },
    initView: function () {
        var item = flax.createDisplay(res.rank, "a47");   //资源库
        this.addChild(item);
        if (!weixinHelper.isOfWeiXin)
            return;
        this.hide(item);
        this.setScript(item);
        this.calCoor(item);
    },
    onExit: function () {
        this._super();
        this.key = null;
        this.sprite = null;
    },
    hide:function(item){
        item['rank1'].visible = false;
        item['icon1'].visible = false;
        item['nickName1'].visible = false;
        item['score1'].visible = false;
        item['leftItem'].visible = false;
    },
    setScript: function (item) {
        var self = this;
        //查看全部排行按钮
        flax.addListener(item['viewBtn'], function () {
            var rank = new rankList(self.key,true,false);
            self.parent.addChild(rank);
        }, InputType.click);
    },
    calCoor:function(item){
        var itemHeight = item['leftItem'].height;
        var itemWidth = item['leftItem'].width;
        //名次//ICON //nickName //score
        var rank = item["rank1"].position.x + "-" + item["rank1"].position.y;
        var icon = item["icon1"].position.x + "-" + item["icon1"].position.y;
        var nickName = item["nickName1"].position.x + "-" + item["nickName1"].position.y;
        var scorePos = item["score1"].position.x + "-" + item["score1"].position.y;
        var leftItem = item['leftItem'].position.x + '-' + item['leftItem'].position.y;
        var message = {
            text: 'showLittleRank',
            rank: rank,
            icon: icon,
            nickName: nickName,
            scorePos: scorePos,
            leftItem: leftItem,
            itemHeight:itemHeight,
            itemWidth:itemWidth,
            key:this.key,
            score:this.score
        };
        this.showCanvas(message);
    },
    showCanvas:function (message) {
        weixinHelper.openDataContext.postMessage(message);
        this.sprite = new PIXI.Sprite();
        this.addChild(this.sprite);
        var btexture = new PIXI.BaseTexture(weixinHelper.sharedCanvas);
        this.sprite.texture = new PIXI.Texture(btexture);
    },
    update:function(){
        var btexture = new PIXI.BaseTexture(weixinHelper.sharedCanvas);
        this.sprite.texture = new PIXI.Texture(btexture);
    },
});

