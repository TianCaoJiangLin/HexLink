/**
 * Created by zhanghua on 2018/5/15.
 * 此处保存所有需要用到枚举的元素，当然是模拟
 */

var allEnum = {
    //六角形的两种方向，水平或者竖直
   orientation:{
       pointy:'pointy',
       flat:"flat"
   },
    //组成的最终图案的形状
    shape:{
        Parallelograms: 'par',
        Triangles:'tri',
        Hexagons:'hex',
        Rectangles:'rec'
    },
    //变体类型
    shapeMode :{
       //平行四边形
        Left:  'left',
        None:  'none',              //正六边形只有none这一种变体
        Right: 'right',
        //三角形
        Up:'up',
        Down:'down'
    },
    //hex六个方向,从右下角(东方位置)开始下标记0,反时针旋转，和方向数组相统一
    //因为这个要参与计算，所以必须用数字值
    hexOrientation:{
        East:0,
        EastNorth:1,
        North:2,
        West:3,
        WestSouth:4,
        South:5
    },
    //转子是敌是我
    owner:{
        None:'none',
        Mine:'mine',
        Enemy:'enemy'
    },
    //用到的颜色
    color:{
        grey:0xCCCCCC,
        skyBlue:0x0099CC,
        blue:0x0099CC,
        grass:0x33CC33,
        orange:0xFF9933,
        red:0xCC3333,
        gold:0xFF9900,
        green:0x006633,
        purple:0x663300,
        black:0x000000,
        white:0xFFFFFF,
    },
    //todo 敌人类型
    enemyType:{
        Normal : 0,
        Defence: -1,
        Silly: -2,
        Attack:1,
        Smart:2
    },
    //关卡游戏结果
    status:{
        None:'none',
        Win:'win',
        Lose:'lose'
    },
    //关卡等级
    levelRank:{
        primary:'pri',
        secondary:'sec',
        adjective:'adj'
    }
};