/**
 * Created by zhanghua on 2018/5/15.
 * 此处定义需要用到的资源
 */

var res = {
    main:"res/main.json",
    main_png:"res/main.png",
    login:"res/login.json",
    login_png:"res/login.png",
    rank:"res/rank.json",
    rank_png:"res/rank.png",
    expand_effect:'res/music/xiaoguo.mp3'
};
var resource = {
    res_main: [
        res.main,
        res.main_png,
        res.expand_effect
    ],
    res_login:[
        res.login,
        res.login_png,
        res.rank,
        res.rank_png
    ],
    res_expand: res.expand_effect

};
