/**
 * Created by zhanghua on 2018/5/25.
 * 此js用来构造所有的容器
 */

//显示六角形的底盘，确定整个图案的形状
var hexBoard = flax.Container.extend({
    onEnter: function () {
        this._super();
        //关卡参数
        var params = [];
        if(flax.userData.userLevel !== -1)
            params = levelUtil.allLevels[flax.userData.userLevel];
        else
            params = levelUtil.getInfinite();
        global.params = params;
        this.initView.apply(this, params);
    },
    //用来显示蜂窝集合，参数分别为方向、形状,大小、哪种变体及行列起止最后两个参数是要挖空的范围
    initView: function (orientation, shape, size, mode, q1, q2, r1, r2, emptyRow, emptyCol,isOneWay) {
        //记录六角形方向，计算点击转动角度时使用
        console.log("关卡关键参数为:",orientation,"__",shape,"___",mode);
        global.orientation = orientation;
        global.isOneWay = isOneWay === undefined ? false : isOneWay;
        hexHelper.allPositon = [];
        hexMap.initAllHex(orientation, shape, size, mode, q1, q2, r1, r2, emptyRow, emptyCol);
        for (var i = 0; i < hexMap.allHex.length; i++) {
            var item = hexMap.allHex[i];
            var baseItem = flax.createDisplay(res.main, "hex");
            this.addChild(baseItem);
            //层次只初始化一次，因为此时没有baseItem,没有放到循环外
            if(i===0)
                hexMap.initSize(baseItem, orientation, shape, size,mode);
            //计算像素坐标
            var _pos = hex_to_pixel(hexMap.layout, item);
            baseItem.position.x = _pos.x;
            baseItem.position.y = _pos.y;
            hexHelper.allPositon.push(baseItem.position);
            baseItem.hexIndex = i;
            //竖直再转向一个角度
            if (orientation === allEnum.orientation.pointy){
                baseItem.rotation += allConst.offset;
            }
            //此处注意，本来是event.currentTarget，现在变成了touch.currentTarget;
            flax.addListener(baseItem, function (touch, event) {
                var item = touch.currentTarget;
                hexHelper.clickItem(item);
            },InputType.click);
        }
    },
    onExit: function () {
        this._super();
    }
});

//显示六角形上的转子
var hexBody = flax.Container.extend({
    onEnter: function () {
        this._super();
        this.initView.apply(this, global.params);
    },
    onExit: function () {
        this._super();
    },
    initView: function (orientation, shape, size, mode, q1, q2, r1, r2) {
        hexHelper.allItems = {};
        hexHelper.allArrows = [];
        for (var i = 0; i < hexMap.allHex.length; i++) {
            var item = hexMap.allHex[i];
            var baseItem = flax.createDisplay(res.main, "a2");
            this.addChild(baseItem);
            baseItem.position.x = hexHelper.allPositon[i].x;
            baseItem.position.y = hexHelper.allPositon[i].y;
            var mes = item.q + "-" + item.r;
            hexHelper.allItems[mes] = baseItem;
            var rand = Math.floor(Math.random() * 6);
            item.orientation = rand;
            baseItem.rotation = - rand * allConst.orientation_offset;
            if (orientation === allEnum.orientation.pointy){
                baseItem.rotation += allConst.offset;
            }
            //颜色
            baseItem["showArrow"].tint = allEnum.color.black;
            baseItem["showHex"].tint = allEnum.color.black;
            hexHelper.allArrows.push(baseItem);
        }
        //计算初始位置
        hexMap.showStartPos(orientation, shape, size, mode, q1, q2, r1, r2);
    }
});

//显示游戏信息
var infoBoard = flax.Container.extend({
    onEnter: function () {
        this._super();
        this.initView();
    },
    onExit: function () {
        this._super();
    },
    initView: function () {
        var item = flax.createDisplay(res.main, "a21");
        this.addChild(item);
        item["levelNum"].text = "" + (flax.userData.userLevel+1);
        hexHelper.enemyRate = item["enemyRateNum"];
        hexHelper.mineRate = item["mineRateNum"];
        hexHelper.scoreTxt = item['scoreNum'];
        hexHelper.enemyRate.text = (100 / hexHelper.allPositon.length).toFixed(1) + "%";
        hexHelper.mineRate.text = (100/ hexHelper.allPositon.length).toFixed(1) + "%";
        hexHelper.scoreTxt.text = "1";
    }
});
//显示胜利或失败面板
var winBoard = flax.Container.extend({
    onEnter: function () {
        this._super();
        this.initView();
    },
    onExit: function () {
        this._super();
    },
    initView: function () {
        var item = flax.createDisplay(res.main, "a64");
        this.addChild(item);
        if (hexHelper.status === allEnum.status.Lose) {
            item["winTitle"].text = "很遗憾，你输了";
            hexHelper.score = 0;
            item["scoreNum"].text = item["scoreNum"].text.replace('{n}',hexHelper.score);
            item["totalNum"].text = item["totalNum"].text.replace('{n}',hexHelper.totalScore);
            hexHelper.scoreTxt = "0";
            item["continueBtn"]['continueTxt'].text = '复活闯关';
        }else{
            //新开始的游戏，从头计数
            if(flax.userData.userLevel === 0)
                hexHelper.totalScore = 0;
            //是否无限关卡
            if(flax.userData.userLevel === -1){
                // item["winTitle"].text = "恭喜你通过最终关卡";
                flax.userData.userLevel = flax.userData.get("userLevel");
                if(!flax.userData.finalMax)
                    flax.userData.finalMax = 0;
                if(flax.userData.finalMax < hexHelper.score)
                    flax.userData.finalMax = hexHelper.score;
                item["totalNum"].text = "最高得分:" + flax.userData.finalMax;
                item["scoreNum"].text = item["scoreNum"].text.replace('{n}',hexHelper.score);
                hexHelper.score = 0;
                flax.userData.save();
                flax.userData.userLevel = -1;
                console.log("无限关卡通关");
            }else{
                item["winTitle"].text = "恭喜你通关!";
                hexHelper.totalScore += hexHelper.score;
                flax.userData.userPoint = hexHelper.totalScore;
                if(flax.userData.maxScore < hexHelper.totalScore)
                    flax.userData.maxScore = hexHelper.totalScore;
                //判断是否到最后一关
                if(flax.userData.userLevel < levelUtil.allLevels.length -1){
                    flax.userData.userLevel ++;
                }else{
                    item.gotoAndStop(1);
                    item['returnBtn']['returnTxt'].text = '返回游戏';
                    item["maxNum"].text = item["maxNum"].text.replace('{n}',flax.userData.maxScore);
                    flax.userData.userLevel = 0;  //已经到达最后一关
                    flax.userData.gameOver = true;      //开启无限关卡
                }
                flax.userData.save();
                item["scoreNum"].text = item["scoreNum"].text.replace('{n}',hexHelper.score);
                item["totalNum"].text = item["totalNum"].text.replace('{n}',hexHelper.totalScore);
                hexHelper.score = 0;
                hexHelper.scoreTxt = "0";
            }
        }
        //返回游戏
        flax.addListener(item['returnBtn'], function () {
          hexHelper.reset();
          flax.replaceScene("login");
        }, InputType.click);

        //接着游戏
        if(item['continueBtn']){
            flax.addListener(item['continueBtn'],function(){
                hexHelper.reset();
                //todo 看视频复活
                flax.replaceScene(flax.userData.userLevel === 0 ? "login" : "main");
            });
        }

    }
});
