/**
 * Created by zhanghua on 2018/5/25.
 * 用来存放所有的全局变量
 */
var global = global || {};
//这里有几个数据直接使用全局变量会方便一点
function initGlobal(){
    global.orientation = null;                            //每一局六角形的方向
    global.isOneWay =  true;                               //是否双向链接
    global.params = [];                                     //关卡参数数组
}
//主要是flax已经自带global变量了，所以这里只能这样修正一下
initGlobal();

