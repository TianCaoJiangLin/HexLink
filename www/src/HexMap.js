/**
 * Created by Administrator on 2018/5/15.
 * 此js主要负责生成图案的数据处理
 * 不同的shape首先有不同方向，相同的方向还会有不同的变体，
 * 例如 平行四边形有 2个方向 * 3种变化共 6种形状
 * 六边形只有2个方向，没有变体，共2种形状
 * 矩形就有 2 个方向 * 6 种变体一共12 种形状，暂未实现
 * 等边三角形只有2个方向 * 2个变体共四种形状
 */
var hexMap = {
    origin_const: Point(allConst.gameWidth / 2, allConst.gameHeight / 2),   //屏幕中心点位置
    origin: null,                                                           //渲染六角形的初始位置（后来调整为和（0，0，0）六角形位置相同，便于计算
    size: 0,                            //蜂窝单元大小，大小是外围正方形边长的一半
    layout: null,                        //排列及计算相关对象
    allHex: [],        //保存所有六角形对象的数组，是数据结构，不是屏幕上的物件
    //敌我双方颜色
    mineColor: allEnum.color.blue,
    enemyColor: allEnum.color.red,
    //六个方向的简写
    _north: allEnum.hexOrientation.North,
    _sourth: allEnum.hexOrientation.South,
    _west: allEnum.hexOrientation.West,
    _east: allEnum.hexOrientation.East,
    _westSouth: allEnum.hexOrientation.WestSouth,
    _eastNorth: allEnum.hexOrientation.EastNorth,
    //六角形两个方向的简写
    _pointy: allEnum.orientation.pointy,
    _flat:allEnum.orientation.flat,

    //获得平行四边形map,参数为六角形方向、起始和终止行列，变体模式，最后两个参数是挖空的地方,横列的半径
    getParallelograms: function (orientation, q1, q2, r1, r2, mode, emptyRow, emptyCol) {
        var result = [];
        if (!emptyRow)
            emptyCol = 0;
        if (!emptyCol)
            emptyCol = 0;
        var flag = false;
        for (var q = q1; q <= q2; q++) {
            for (var r = r1; r <= r2; r++) {
                flag = false;                       //每个循环都要重置
                for (var j = -emptyRow + 1; j < emptyRow; j++) {
                    var flag1 = q == j;
                    var flag2 = -emptyCol < r && r < emptyCol;
                    if (flag1 && flag2) {
                        flag = true;
                        break;
                    }
                }
                if (flag)
                    continue;               //如果挖空，跳过
                result.push(this.getHexCoor(orientation, q, r, mode));
            }
        }
        return result;
    },
    //获得正六边形map,正六边形没有变体，参数是方向、半径、挖洞的半径
    getHexagons: function (orientation, radius, emptyRow) {
        if (emptyRow == undefined)
            emptyRow = 0;
        var result = [];
        var flag = false;
        for (var q = -radius; q <= radius; q++) {
            var r1 = Math.max(-radius, -q - radius);
            var r2 = Math.min(radius, -q + radius);
            for (var r = r1; r <= r2; r++) {
                flag = false;
                if (emptyRow > 0) {
                    for (var j = -emptyRow; j <= emptyRow; j++) {
                        var flag1 = q === j;
                        var r11 = Math.max(-emptyRow, -j - emptyRow);
                        var r22 = Math.min(emptyRow, -j + emptyRow);
                        var flag2 = r11 <= r && r <= r22
                        if (flag1 && flag2) {
                            flag = true;
                            break;
                        }
                    }
                }
                if (flag)
                    continue;
                result.push(this.getHexCoor(orientation, q, r));
            }
        }
        return result;
    },
    //正三角形，正三角形的变体只和y轴向上或向下有关
    //参数为:方向，边长 、Y轴方向
    //2个方向 * 两种变体共四种形状
    //这里要做一下修改，因为它不是中心对称的，所以要固定一个（0，0）点便于计算中心位置，
    //虽然这样生成规则要复杂一些
    getTriangles: function (orientation, size, mode) {
        var result = [];
        var isFlat = orientation === this._flat;
        var isUp = mode === allEnum.shapeMode.Up;
        if (isFlat) {
            if (isUp) {
                for (var q = 0; q < size; q++)
                    for (var r = 1 - size; r <= -q; r++)
                        result.push(this.getHexCoor(orientation, q, r));
            } else {
                for (var q = 0; q > -size; q--)
                    for (var r = -q; r < size; r++)
                        result.push(this.getHexCoor(orientation, q, r));
            }
        } else {
            if (isUp) {
                //这里 Q向下为正,r向左为正 切记
                for (var q = 0; q < size; q++)
                    for (var r = 0; r >= -q; r--)
                        result.push(this.getHexCoor(orientation, q, r));
            } else {
                for (var q = 0; q > -size; q--)
                    for (var r = 0; r <= -q; r++)
                        result.push(this.getHexCoor(orientation, q, r));
            }
        }
        //这是原来不考虑中心点坐标的生成方法
        // if(isFlat === isUp){
        //     for (var q = 0; q < size; q++)
        //         for (var r = 0; r < size - q; r++)
        //             result.push(this.getHexCoor(orientation, q, r));
        // }else{
        //     for (var q = 0; q > -size; q--)
        //         for (var r = -q ; r < size; r++)
        //             result.push(this.getHexCoor(orientation, q, r));
        // }
        return result;
    },
    //矩形，六种变体,
    //todo 在getHexCoor方法上表现不同
    //功能未完全实现，可以单独为矩形6个变种及2个方向共计12个形状写一个类似getHexCoor方法
    getRectangles: function (orientation, q1, q2, r1, r2, mode) {
        var result = [];
        for (var r = 0; r <= q2 - q1 + 1; r++) {
            var r_offset = Math.floor(r / 2);
            for (var q = -r_offset; q < r2 - r1 + 1 - r_offset; q++) {
                result.push(this.getHexCoor(orientation, q, r, mode));
            }
        }
        return result;
    },
    //初始化所有蜂窝坐标
    initAllHex: function (orientation, shape, size, mode, q1, q2, r1, r2, emptyRow, emptyCol) {
        switch (shape) {
            case allEnum.shape.Parallelograms:
                this.allHex = this.getParallelograms(orientation, q1, q2, r1, r2, mode, emptyRow, emptyCol);
                break;
            case allEnum.shape.Triangles:
                this.allHex = this.getTriangles(orientation, size, mode);
                break;
            case allEnum.shape.Rectangles:
                this.allHex = this.getRectangles(orientation, q1, q2, r1, r2, mode);
                break;
            case allEnum.shape.Hexagons:
            default:
                this.allHex = this.getHexagons(orientation, size, emptyRow);
                break;
        }
    },
    //初始化蜂窝大小和相关布局数据
    initSize: function (baseItem, orientation, shape, size, mode) {
        var r = Math.floor(baseItem.width / 2) + 3;     //3是调整边距用的
        //这里的x,y是边长的一半加3，不是整个边长，切记
        this.size = {
            x: r,
            y: r
        };
        this.origin = {};
        this.origin.x = this.origin_const.x;
        this.origin.y = this.origin_const.y;
        //三角形时要重新设置中心点,和它mode相关
        if (shape === allEnum.shape.Triangles)
            this.calTriOrigin(orientation, size, mode);
        //todo 矩形时也要单独设置中心点，
        this.layout = Layout(allConst[orientation], this.size, this.origin);
    },
    calTriOrigin: function (orientation, size, mode) {
        var isFlat = orientation === this._flat;
        var isUp = mode === allEnum.shapeMode.Up;
        var step = this.size.x * (2 - 1 / Math.sqrt(3)) / 2;
        var _x = 0;
        var _y = 0;
        //竖直方向初始旋转导致的坐标差
        var offsetX = this.size.x / Math.sqrt(3) * 0.5;
        var offsetY = this.size.x * (2 / Math.sqrt(3) - 1) * 0.5;
        if (!isFlat) {
            if (isUp) {
                _x = 0 + offsetX;
                _y = -(size - 1) * step + offsetY;
            } else {
                _x = 0 + offsetX;
                _y = (size - 1) * step + offsetY;
            }
        } else {
            if (isUp) {
                _x = (size - 1) * step;
                _y = 0;
            } else {
                _x = -(size - 1) * step;
                _y = 0;
            }
        }
        this.origin.x += _x;
        this.origin.y += _y;
    },
    //根据遍历的元素不同,分别为(qr,rs,qs)循环，得到变体
    //返回一个Hex对象，参数分别为方向， q r 坐标 变体
    getHexCoor: function (orientation, q, r, mode) {
        var x, y, z;
        switch (mode) {
            case allEnum.shapeMode.None:
                if (orientation === this._flat) {
                    x = -q - r;
                    y = q;
                    z = r;
                } else {
                    x = r;
                    y = -q - r;
                    z = q;
                }
                break;
            case allEnum.shapeMode.Left:
                x = q;
                y = r;
                z = -q - r;
                break;
            case allEnum.shapeMode.Right:
            default:
                if (orientation === this._flat) {
                    x = r;
                    y = -q - r;
                    z = q;
                } else {
                    x = -q - r;
                    y = q;
                    z = r;
                }
                break;
        }
        ;
        return Hex(x, y, z);
    },
    //计算我方和敌方的初始转子位置 参数分别为方向、形状、变体、大小
    calStartHex: function (orientation, shape, size, mode, q1, q2, r1, r2) {
        var result;
        switch (shape) {
            case allEnum.shape.Parallelograms:
                result = this.calParalStart(orientation, mode, q1, q2, r1, r2);
                break;
            case allEnum.shape.Triangles:
                result = this.calTriangleStart(orientation, mode, size);
                break;
            case allEnum.shape.Rectangles:
                this.calRectangleStart(orientation,mode,q1, q2, r1, r2);
                break;
            case allEnum.shape.Hexagons:
            default:
                result = this.calHexagousStart(orientation, size);
                break;
        }
        return result;
    },
    //计算平行四边形初始位置
    calParalStart: function (orientation, mode, q1, q2, r1, r2) {
        var result = {
            mine: null,
            enemy: null
        };
        switch (mode) {
            case allEnum.shapeMode.Left:
                result.enemy = Hex(q1, r1, -q1 - r1);
                result.mine = Hex(q2, r2, -q2 - r2);
                break;
            case allEnum.shapeMode.Right:
                if (orientation === this._flat) {
                    result.mine = Hex(r1, -r1 - q1, q1);
                    result.enemy = Hex(r2, -r2 - q2, q2);
                } else {
                    result.mine = Hex(-q2 - r1, q2, r1);
                    result.enemy = Hex(-q1 - r2, q1, r2);
                }
                break;
            case allEnum.shapeMode.None:
            default:
                if (orientation === this._flat) {
                    result.enemy = Hex(-q2 - r2, q2, r2);
                    result.mine = Hex(-q1 - r1, q1, r1);
                } else {
                    result.mine = Hex(r1, -r1 - q1, q1);
                    result.enemy = Hex(r2, -r2 - q2, q2);
                }
                break;
        }
        return result;
    },
    //todo 计算矩形初始位置
    calRectangleStart:function(orientation, mode, q1, q2, r1, r2){

    },
    //计算三角形初始位置
    calTriangleStart: function (orientation, mode, size) {
        console.log(orientation, mode, size);
        var result = {
            mine: null,
            enemy: null
        };
        var isFlat = orientation === this._flat;
        var isUp = mode === allEnum.shapeMode.Up;
        if (isFlat) {
            if (isUp) {
                result.mine = Hex(0, 0, 0);       //q,r s
                result.enemy = Hex(1 - size, 0, size - 1);
            } else {
                result.mine = Hex(size - 1, 0, 1 - size);
                result.enemy = Hex(size - 1, 1 - size, 0);
            }
        } else {
            //这里可以看出在_point状态下，mine和enemy是对角交换的关系
            if (isUp) {
                result.mine = Hex(0, size - 1, 1 - size);
                result.enemy = Hex(1 - size, size - 1, 0);
            } else {
                result.mine = Hex(size - 1, 1 - size, 0);
                result.enemy = Hex(0, 1 - size, size - 1);
            }
        }
        return result;
    },
    //计算六边形初始位置
    calHexagousStart: function (orientation, size) {
        var result = {
            mine: null,
            enemy: null
        };
        if (orientation === this._flat) {
            result.mine = Hex(0, size, -size);
            result.enemy = Hex(0, -size, size);
        } else {
            console.log("point");
            result.enemy = Hex(-size, 0, size);
            result.mine = Hex(size, 0, -size);
        }
        return result;
    },
    //计算并显示敌我双方开始位置
    showStartPos: function (orientation, shape, size, mode, q1, q2, r1, r2) {
        var startPos = hexMap.calStartHex(orientation, shape, size, mode, q1, q2, r1, r2);
        var ourHex = hexHelper.getHexByCoor(startPos.mine, hexMap.allHex);
        var enemyHex = hexHelper.getHexByCoor(startPos.enemy, hexMap.allHex);
        ourHex.owner = allEnum.owner.Mine;
        enemyHex.owner = allEnum.owner.Enemy;
        hexHelper.enemyPos.push(enemyHex);
        var enemyMes = enemyHex.q + "-" + enemyHex.r;
        var ourMes = ourHex.q + "-" + ourHex.r;
        hexHelper.allItems[ourMes]["showHex"].tint = this.mineColor;
        hexHelper.allItems[ourMes]["showArrow"].tint = this.mineColor;
        hexHelper.allItems[enemyMes]["showHex"].tint = this.enemyColor;
        hexHelper.allItems[enemyMes]["showArrow"].tint = this.enemyColor;
        this.initStartRotation(orientation, shape, size, mode, q1, q2, r1, r2, ourHex, enemyHex);
    },
    //重新计算初始位置的旋转
    initStartRotation: function (orientation, shape, size, mode, q1, q2, r1, r2, ourHex, enemyHex) {
        switch (shape) {
            case allEnum.shape.Hexagons:
                this.initHexStartRotation(orientation, ourHex, enemyHex);
                break;
            case allEnum.shape.Parallelograms:
                this.initParalStartRotation(orientation, mode, q1, q2, r1, r2, ourHex, enemyHex);
                break;
            case allEnum.shape.Triangles:
                this.initTriangleRotation(orientation, mode, ourHex, enemyHex);
                break;
            case allEnum.shape.Rectangles:
            default:
                this.initRectangleRotation();
                break;
        }
    },
    //正六边形的初始位置的旋转的计算
    initHexStartRotation: function (orientation, ourHex, enemyHex) {
        var isPointy = orientation === this._pointy;
        if (isPointy)
            this.initRotation(ourHex, this._sourth, enemyHex, this._north, isPointy);
        else
            this.initRotation(ourHex, this._westSouth, enemyHex, this._eastNorth, isPointy);
    },
    //平行四边形的初始位置的旋转的计算,需要计算6种情况其中left变体两种方向结果相同
    initParalStartRotation: function (orientation, mode, q1, q2, r1, r2, ourHex, enemyHex) {

        var isPointy = orientation === this._pointy;
        switch (mode) {
            case allEnum.shapeMode.Left:
                this.initRotation(ourHex, this._westSouth, enemyHex, this._eastNorth, true);
                break;
            case allEnum.shapeMode.Right:
                if (isPointy)
                    this.initRotation(ourHex, this._westSouth, enemyHex, this._eastNorth, isPointy);
                else
                    this.initRotation(ourHex, this._west, enemyHex, this._east, isPointy);
                break;
            case allEnum.shapeMode.None:
            default:
                if (isPointy)
                    this.initRotation(ourHex, this._west, enemyHex, this._east, isPointy);
                else
                    this.initRotation(ourHex, this._sourth, enemyHex, this._north, isPointy);
                break;
        }
    },
    initRotation: function (ourHex, ourOri, enemyHex, eneOri, isPointy) {
        var enemyMes = enemyHex.q + "-" + enemyHex.r;
        var ourMes = ourHex.q + "-" + ourHex.r;
        var offset = isPointy ? allConst.offset : 0;
        ourHex.orientation = ourOri;
        enemyHex.orientation = eneOri;
        hexHelper.allItems[ourMes].rotation = -ourOri * allConst.orientation_offset + offset;
        hexHelper.allItems[enemyMes].rotation = -eneOri * allConst.orientation_offset + offset;
    },
    //三角形初始位置的旋转的计算
    initTriangleRotation: function (orientation, mode, ourHex, enemyHex) {
        var isPointy = orientation === this._pointy;
        switch (mode) {
            case allEnum.shapeMode.Up:
                if (isPointy)
                    this.initRotation(ourHex, this._westSouth, enemyHex, this._north, isPointy);
                else
                    this.initRotation(ourHex, this._sourth, enemyHex, this._eastNorth, isPointy);
                break;
            case allEnum.shapeMode.Down:
            default:
                if (isPointy)
                    this.initRotation(ourHex, this._sourth, enemyHex, this._eastNorth, isPointy);
                else
                    this.initRotation(ourHex, this._westSouth, enemyHex, this._east, isPointy);

                break;
        }

    },
    //todo 矩形12种初始位置的旋转的计算
    //todo 矩形图案生成还未实现
    initRectangleRotation: function () {

    }
};
