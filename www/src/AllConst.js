/**
 * Created by zhanghua on 2018/5/25.
 * 用来存放所有常量
 */

//因为相互耦合的原因，这个方法从Hex.js中单独提了出来并全新命名
function _Orientation(f0, f1, f2, f3, b0, b1, b2, b3, start_angle) {
    return {f0: f0, f1: f1, f2: f2, f3: f3, b0: b0, b1: b1, b2: b2, b3: b3, start_angle: start_angle};
}

var allConst = {
    gameWidth:750,           //游戏的大小
    gameHeight:1206,
    offset:- Math.PI / 6,              // point_hex的旋转角度
    orientation_offset:Math.PI/3,       //每个方向对应的角度差
    //六角形的方向
    pointy:_Orientation(Math.sqrt(3), Math.sqrt(3) / 2, 0, 3 / 2, Math.sqrt(3) / 3, -1 / 3, 0, 2 / 3, 0.5),
    flat:_Orientation(3 / 2, 0, Math.sqrt(3) / 2, Math.sqrt(3), 2 / 3, 0, -1 / 3, Math.sqrt(3), 0),
    weixinKey:'score'       //微信存储主键
};