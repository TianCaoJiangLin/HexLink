/**
 * Created by zhanghua on 2018/5/18.
 * 本js 用来存放所有关卡数据
 */

/**
 *
 * 参数数组内容1、六角形方向 2、图形形状
 * 3、图案大小，如果是正六边形，主要是半径
 * 4，变体模式（左右倾斜，三角形还有上下等）
 * 5-8是平行四边形时的四个边界，（q1,q2,r1,r2)其中q1<q2,r1<r2
 * 9,10两个参数表示挖空的范围,如果形状是六边形，第一个参数表示挖空的半径，第二个参数表示挖空的位置
 * 如果形状是平行四边表，代表挖空的行列，目前先只设计挖空一处
 * 需要增加参数，1、敌方行动类型，分别为：正常形（在扩展空地和进攻之间随机，50%），防守形（除非被进攻才反击，不然空地占完了才进攻），挨打型（空地不能占了才进攻） ，
 * 进攻型（不能进攻了才占空地），聪明型（进攻或者占空地时预先计算能占的大小，取最多）
 * 以上均只计算一步的情况
 * 现阶段没法计算两步，
 */
//关卡参数 todo 增加矩形
var levelUtil = {
    //几个同名变量,简化输入
    //方向
    _pointy: allEnum.orientation.pointy,
    _flat: allEnum.orientation.flat,
    //形状
    _par: allEnum.shape.Parallelograms,
    _hex: allEnum.shape.Hexagons,
    _tri: allEnum.shape.Triangles,
    _rec: allEnum.shape.Rectangles,
    //朝向
    _left: allEnum.shapeMode.Left,
    _right: allEnum.shapeMode.Right,
    _none: allEnum.shapeMode.None,
    _up: allEnum.shapeMode.Up,
    _down: allEnum.shapeMode.Down,
    //最终生成的关卡
    allLevels: [],
    //初始关卡，选择一半
    getPrimary:function(){
        return [
            //平行四边形
            [this._pointy, this._par, 2, this._right, -2, 2, -2, 2, 1, 1],
            [this._pointy, this._par, 2, this._left, -2, 2, -2, 2, 1, 1],
            [this._pointy, this._par, 2, this._none, -2, 2, -2, 2, 1, 1],
            [this._flat, this._par, 2, this._right, -2, 2, -2, 2, 1, 1],
            [this._flat, this._par, 2, this._left, -2, 2, -2, 2, 1, 1],
            [this._flat, this._par, 2, this._none, -2, 2, -2, 2, 1, 1],
            //三角形 暂未实现挖空
            [this._flat, this._tri, 7, this._up, -2, 2, -2, 2, 1, 1],
            [this._flat, this._tri, 7, this._down, -2, 2, -2, 2, 1, 1],
            [this._pointy, this._tri, 7, this._up, -2, 2, -2, 2, 1, 1]
            [this._pointy, this._tri, 7, this._down, -2, 2, -2, 2, 1, 1]
        ];
    } ,
    //中级关卡，全部选择
    getSecondary: function() {
        return [
                [this._flat, this._hex, 3, this._none, -5, 5, -5, 5, 1, 1],
                [this._pointy, this._hex, 3, this._none, -5, 5, -5, 5, 1, 1],
                [this._flat, this._par, 2, this._right, -3, 3, -3, 3, 1, 1],
                [this._pointy, this._par, 2, this._none, -3, 3, -3, 3, 1, 1],
                [this._flat, this._par, 2, this._left, -3, 3, -3, 3, 1, 1]
               ];
    },
    //高级关卡,全部选择
    getAdjective:function(){
        return [];
    },
    getInfinite:function(){
        return [this._flat, this._hex, 4, this._none, -5, 5, -5, 5, 0, 0,true];
    },
    //初始化关卡,参数为关卡等级
    initLevel: function (rank) {
        var _indexes = [];
        var _source = [];
        var _rate = 1;
        switch (rank) {
            case allEnum.levelRank.primary:
                _source = this.getPrimary();
                _rate = config.primaryRate;
                break;
            case allEnum.levelRank.secondary:
                _source = this.getSecondary();
                _rate = config.secondaryRate;
                break;
            case allEnum.levelRank.adjective:
            default:
                _source = this.getAdjective();
                _rate = config.adjectiveRate;
                break;
        }
        var _len = _source.length;
        while (_indexes.length < Math.ceil(_len * _rate)) {
            var _index = parseInt(Math.random() * _len);
            if (_indexes.indexOf(_index) === -1) {
                _indexes.push(_index);
                this.allLevels.push(_source[_index]);
            }
        }
    },

    //初始化所有关卡
    initLevelAll: function () {
        for (var key in allEnum.levelRank)
            this.initLevel(allEnum.levelRank[key]);
    }
};


