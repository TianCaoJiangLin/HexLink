/**
 * Created by zhanghua on 2018/5/25.
 * 用来存放游戏可以调节的数据
 */
var config  = {
    primaryRate:0.5,            //初始关卡选取比例
    secondaryRate:1,            //中级关卡选取比例
    adjectiveRate:1,            //中级关卡选取比例
    winRate:0.7,            //占有率多少可赢
    divisor:Math.PI/40,         //转子动画时每次转动的角度
    timeout:40,           //转子动画时间隔的毫秒数
    delayTime:300     //指向链之间的间隔 毫秒
};