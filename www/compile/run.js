//https://github.com/javascript-obfuscator/javascript-obfuscator#deadcodeinjection
var JavaScriptObfuscator = require('javascript-obfuscator');
var fs = require("fs");
var path = require("path");
var child_process = require('child_process');

var publishFolder = "../publish";
//publishFolder = "../test";

var obscure = false;
if(publishFolder == "../test") obscure = false;

var indexFile = "../index.html";
var sourceFolders = ["res"];

var lockedDomain = null;//"etdogs.com"; //锁定域名
var onlyMobile = true;

var indexStr = fs.readFileSync(indexFile, "utf8");
var reg = /<script src="(.+?)><\/script>/g;
var scripts = indexStr.match(reg);
indexStr = indexStr.replace(reg, "");

//清空创建发布文件夹
deleteFolderRecursive(publishFolder);
fs.mkdirSync(publishFolder);

var outJsName = "gameCore.js";

indexStr = indexStr.replace('</head>',  '<script src="' + outJsName + '"></script></head>');
fs.writeFileSync(publishFolder + "/index.html", indexStr);

for(var i = 0; i < sourceFolders.length; i++) {
    copyDir("../" + sourceFolders[i], publishFolder + "/" + sourceFolders[i]);
}

console.log("start google complier....")

var compiler = "./compiler-1.6.jar";

googleCompile(true);

function googleCompile(isLib, jsStr) {
    var jsList = [];
    for(var i = 0; i < scripts.length; i++) {
        var s = scripts[i].match(/"(.+?)"/)[0];
        s = s.replace(/"/g, "");
        s = "." + s;

        if(isLib && s.indexOf("lib/") == -1) {
            continue;
        }
        if(!isLib && s.indexOf("lib/") > -1) {
            continue;
        }

        console.log(s);
        jsList.push(s);
    }

    jsList = jsList.join(" ");

    outJsName = publishFolder + "/" + outJsName;

//压缩等级
    var compileLevel = "SIMPLE_OPTIMIZATIONS";//[WHITESPACE_ONLY | SIMPLE_OPTIMIZATIONS | ADVANCED_OPTIMIZATIONS]
//--create_source_map ' + (publishFolder + '/main.min.js.map')
    var cmdStr = 'java -jar ' + compiler + ' --language_in=ECMASCRIPT5 --warning_level=quiet  --debug=false '  + ' --compilation_level ' + compileLevel + ' --js_output_file ' + outJsName + " " + jsList;

    var exec = child_process.exec(cmdStr);

    exec.stdout.on('data', function (info) {
        if(info.length > 1) console.log(info);
    });
    exec.stderr.on('data', function (err) {
        if(err.length > 1) console.log(err);
        process.exit();

    });
    exec.on('exit', function (code) {
        console.log("--------over--------");

        var fileStr = fs.readFileSync(outJsName, "utf8");

        if(isLib) {
            googleCompile(false, fileStr);
        } else {
            if(lockedDomain) {
                var lockScript = '$(function(){' +
                    'if(document.domain.indexOf("'
                    + lockedDomain
                    + '") == -1) ' +
                    '{'  +
                    '$("body").empty();' +
                    '}' +
                    '}' +
                    ')'

                //锁定域名
                fileStr = fileStr + lockScript;
            }
            //if(onlyMobile) {
            //    //TODO
            //    var onlyMobileScript =
            //    'window.mobileAndTabletcheck = function() {' +
            //     +'var check = false;' +
            //     +'(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);'
            //     +'   return check;'
            //    +'};' +
            //    'if(!mobileAndTabletcheck()){alert("fuck")}';
            //    console.log(onlyMobileScript);
            //    fileStr = fileStr + onlyMobileScript;
            //}
            if(obscure) fileStr = obfuscation(fileStr);
            fs.writeFileSync(outJsName, jsStr + fileStr);
        }
    });
}

function obfuscation(jsStr) {
    var options =  {
        compact: true,

        //设为true，threshHold是0-1，越大文件越大，效率越低
        controlFlowFlattening: true,
        controlFlowFlatteningThreshold: 0.75,

        //设为true，注入垃圾代码，threshHold是0-1，越大文件越大，效率越低
        deadCodeInjection: true,
        deadCodeInjectionThreshold: 0.75,

        //禁止本地编译
        debugProtection: true,
        debugProtectionInterval: true,
        disableConsoleOutput: true,

        identifierNamesGenerator: 'hexadecimal',
        log: false,
        renameGlobals: false,
        rotateStringArray: true,
        //放置代码被格式化，貌似用处不大
        selfDefending: true,

        //导致页面解析变慢
        //rc4比base64还慢, 文件略大, 769k到967K
        //测试文件分别是base64, rc4, 都不要， selfdefending为false
        //文件大小769, 967, 671,670，事实证明，不要stringArray的话，全是明码，对比下rc4和base64好了
        stringArray: true,
        stringArrayEncoding: "rc4",//"rc4",//'rc4' or base64
        stringArrayThreshold: 0.75,

        transformObjectKeys: true,
        unicodeEscapeSequence: false
    }

    var obfuscationResult = JavaScriptObfuscator.obfuscate(jsStr, options);
    return obfuscationResult.getObfuscatedCode();
}

//删除文件夹
function deleteFolderRecursive(path) {
    if( fs.existsSync(path) ) {
        fs.readdirSync(path).forEach(function(file) {
            var curPath = path + "/" + file;
            if(fs.statSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};

/*
 * 复制目录、子目录，及其中的文件
 * @param src {String} 要复制的目录
 * @param dist {String} 复制到目标目录
 */
function copyDir(src, dist, callback) {
    fs.access(dist, function(err){
        if(err){
            // 目录不存在时创建目录
            fs.mkdirSync(dist);
        }
        _copy(null, src, dist);
    });

    function _copy(err, src, dist) {
        if(err){
            callback(err);
        } else {
            fs.readdir(src, function(err, paths) {
                if(err){
                    callback(err)
                } else {
                    paths.forEach(function(path) {
                        var _src = src + '/' +path;
                        var _dist = dist + '/' +path;
                        fs.stat(_src, function(err, stat) {
                            if(err){
                                callback(err);
                            } else {
                                // 判断是文件还是目录
                                if(stat.isFile()) {
                                    fs.writeFileSync(_dist, fs.readFileSync(_src));
                                } else if(stat.isDirectory()) {
                                    // 当是目录是，递归复制
                                    copyDir(_src, _dist, callback)
                                }
                            }
                        })
                    })
                }
            })
        }
    }
}