/**
 * Created by Administrator on 2018/5/22.
 * 子域主页面
 */

import pageList from 'pageList';              //实现分页功能
import drawHelper from 'drawHelper';            //绘制模块
import scoreHelper from 'scoreHelper';          //拖管数据模块
import userHelper from 'userHelper';            //用户自身数据模块
drawHelper.init();
//监听消息
wx.onMessage( data =>{
    switch (data.text) {
        case 'initScore':
            scoreHelper.initScore(data.key,data.value);
            break;
        case 'prePage':
            nextPage(false);
            break;
        case 'nextPage':
            nextPage(true);
            break;
        case 'exitRank':
            exitRank(data);
            break;
        case 'getUserInfo':
            userHelper.getUserInfo();
            break;
        case 'showLittleRank':
            scoreHelper.setScore(data.key,data.score,getFriendClouds,data,true);
            break;
        case 'showAllRank':
            showAllRank(data);
            break;
        case 'getGroupCloudStorage':
            //获得群排行榜
            getGroupClouds(data);
            break;
        case 'getFriendCloudStorage':
        default:
            getFriendClouds(data,false);
            break;
    }
});

//上/下一页
function nextPage(isNext){
    var result = isNext?pageList.nextPage():pageList.prePage();
    if(result!=null){
        drawHelper.clearCanvas();
        drawHelper.refreshVec(result);
        drawHelper.drawVecItem(userHelper.userInfo,0,true);
    }
}

//关掉排行榜
function exitRank(data) {
    drawHelper.clearCanvas();
    pageList.currentPage = 1;
    pageList.pageNum = 1;
    userHelper.userInfo = null;
    if(data.refresh){
        // 重绘
        drawHelper.refreshHor(pageList.horList);
    }else{
        //重置数据
        pageList.userList.length = 0;
    }
}
//好友排行榜
//参数分别为传过来的数据和是否小排行榜
function getFriendClouds(data, isLittleRank) {
    if(isLittleRank)
        drawHelper.setHorPos(data);
    else
        drawHelper.setVecPos(data);

    wx.getFriendCloudStorage({
        keyList: [data.key],
        success:res =>{
            initRank(data.key,res, isLittleRank);
        },
        fail: res => {
            console.log(res);
        }
    });
}
//群排行榜
function getGroupClouds(data) {
    drawHelper.setVecPos(data);
    wx.getGroupCloudStorage({
        shareTicket: data.ticket,
        keyList: [data.key],
        success: res =>{
            console.log(res);
            initRank(data.key,res,false);
        },
        fail: res =>{
            console.log(res);
        }
    });
}
//排序并初始化和绘制各排行榜内容
function initRank(key,res, isLittleRank) {
    //排序
    scoreHelper.setKey(key);
    //增加测试数据
    // pageList.addTestData(11,res.data);
    res.data.sort(scoreHelper.compareKVLis);
    userHelper.setUserItem(res.data);
    pageList.userList = res.data;
    drawHelper.clearCanvas();
    if (isLittleRank) {
        pageList.horList = pageList.getHorArray(userHelper.userInfo);
       drawHelper.refreshHor(pageList.horList);
    } else {
        pageList.setPageNum();
        drawHelper.refreshVec(pageList.pageView());
        //绘制自己的排名
        drawHelper.drawUser(userHelper.userInfo);
    }
}

//显示全部排行榜，不需要重新获取数据
function showAllRank(data){
    drawHelper.clearCanvas();
    getFriendClouds(data,false);
}




