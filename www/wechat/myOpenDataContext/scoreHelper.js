/**
 * Created by Administrator on 2018/5/31.
 * 这里主要进行托管数据的处理
 */
var scoreHelper = {
    key: "",                 //比较的key
    maxScore: 0,             //最高记录
    setKey: function (key) {
        this.key = key;
    },
    //todo
    //排序比较方法
    //默认比较第一个参数，可以改进比较任何key
    //在回调里面，this 不是原对象，切记
    compareKVLis: function (i0, i1) {
        //todo或者剔除掉
        //处理测试数据,上线前删除掉
        if (i0.KVDataList.length == 0) {
            i0.KVDataList.push({
                key: scoreHelper.key,
                value: "0"
            });
        }
        if (i1.KVDataList.length == 0) {
            i1.KVDataList.push({
                key: scoreHelper.key,
                value: "0"
            });
        }
        var x = parseInt(i0.KVDataList[0].value);
        var y = parseInt(i1.KVDataList[0].value);
        if (x < y)
            return 1;
        else if (x == y)
            return 0;
        else
            return -1;
    },
    //每次运行判断有没有初始化过拖管数据
    //有就跳过
    initScore: function (key, value) {
        var self = this;
        var keyList = [];
        keyList.push(key);
        var flag = false;
        wx.getUserCloudStorage({
            keyList: keyList,
            success: (res) => {
                if (res.KVDataList.length != 0) {
                    for (var i = 0; i < res.KVDataList.length; i++) {
                        if (res.KVDataList[i].key == key) {
                            //存在对应的key
                            this.maxScore = res.KVDataList[i].value;
                            flag = true;
                            break;
                        }
                    }
                }
                //没有初始过托管数据，进行初始化
                if (!flag) {
                    console.log("未进行过初始化，正在初始化");
                    var kvDataList = new Array();
                    kvDataList.push({
                        key: key,
                        value: "" + value
                    });
                    wx.setUserCloudStorage({
                        KVDataList: kvDataList,
                        success: () => {
                            this.maxScore = value;
                        }
                    });
                }else{
                    console.log("已经初始化过,跳过初始化，最高记录为: " + this.maxScore);
                }
            }
        });
    },
    // 设置最高分，并且刷新小排行榜
    //用在游戏结束时调用
    setScore: function (key, score, callBack, param1, param2) {
        if (score < this.maxScore) {
            if (callBack)
                callBack(param1, param2);
        } else {
            var kvDataList = new Array();
            kvDataList.push({
                key: key,
                value: "" + score
            });
            wx.setUserCloudStorage({
                KVDataList: kvDataList,
                success: () => {
                    this.maxScore = score;
                    //todo 显示maxScore
                    console.log("最高记录设置成功");
                    if (callBack)
                        callBack(param1, param2);
                }
            });
        }
    }
};
export default scoreHelper;
