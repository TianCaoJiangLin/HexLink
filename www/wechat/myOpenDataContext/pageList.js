/**
 * Created by Administrator on 2018/5/31.
 * 这里主要进行一些分页操作
 */
var pageList = {
    userList: [],                //所有的用户列表
    currentPage: 1,            //当前页，页码大小，总页码
    pageSize: 5,
    pageNum: 1,
    horList: [],      //横向小排行榜
    hasNextPage: function () {
        return this.currentPage < this.pageNum;
    },
    hasPrePage: function () {
        return this.currentPage > 1;
    },
    setPageNum: function () {
        this.pageNum = Math.ceil(this.userList.length / this.pageSize);
    },
    //下面两个方法为增加测试数据
    addTestData:function(num,data){
        for(var j =0;j<num;j++){
            var result =this.clone(data[0],j);
            data.push(result);
        }
    },
    clone:function(item,j){
        var result = {
            nickname:"test" + j,
            avatarUrl:item.avatarUrl,
            KVDataList:[]
        };
        var kv = {
            key:item.KVDataList[0].key,
            value:""+ Math.ceil(Math.random() * 1000)
        };
        result.KVDataList.push(kv);
        return result;
    },
    //分页显示
    pageView: function () {
        var pageItem = [];
        //名次偏移量
        var offset = this.pageSize * (this.currentPage - 1);
        for (var i = 0; i < this.pageSize; i++) {
            var j = i + offset;
            if (j >= this.userList.length)
                break;
            this.userList[j].index = j;
            pageItem.push(this.userList[j]);
        }
        return pageItem;
    },
    nextPage: function () {
        if (this.hasNextPage()) {
            this.currentPage++;
            return this.pageView()
        } else
            return null;
    },
    prePage: function () {
        if (this.hasPrePage()) {
            this.currentPage--;
            return this.pageView();
        } else
            return null;
    },
    //得到水平方向排行榜的用户数组
    getHorArray: function (userInfo) {
        var result = [];
        if (userInfo.index == 0) {
            for (var j = 0; j < 3; j++) {
                if (j >= this.userList.length) {
                    break;
                }

                this.userList[j].index = j;
                result.push(this.userList[j]);
            }
        } else {
            for (var j = -1; j < 2; j++) {
                var k = j + userInfo.index;
                if (k >= this.userList.length) {
                    break;
                }
                this.userList[k].index = k;
                result.push(this.userList[k]);
            }
        }
        return result;
    }
};
export default pageList;