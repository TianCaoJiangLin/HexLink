/**
/**
 * Created by Administrator on 2018/5/31.
 * 这里处理和用户自己相关的数据
 */
var userHelper = {
    userInfo: null,               //返回的自己的数据
    openId: "",                 //用于授权之后的进一步操作
    avatarUrl: "",                //用户头像url
    //得到用户基本数据，无需授权，主要是得到icon url
    getUserInfo: function () {
        wx.getUserInfo({
            openIdList: ['selfOpenId'],
            lang: 'zh_CN',
            success: res => {
                this.avatarUrl = res.data[0].avatarUrl;
            },
            fail: res => {
                // console.log(res);
            }
        });
    },
    //遍历查找用户自己,用avatarUrl判断相等
    setUserItem: function (userList) {
        var result = null;
        for (var i = 0; i < userList.length; i++) {
            if (userList[i].avatarUrl == this.avatarUrl) {
                result = userList[i];
                result.index = i;
                break;
            }
        }
        this.userInfo = result;
        return result;
    }
};
export default userHelper;