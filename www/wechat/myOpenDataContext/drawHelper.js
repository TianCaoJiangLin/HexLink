/**
 * Created by Administrator on 2018/5/31.
 * 这里进行一些绘制排行榜的操作
 */
var drawHelper = {
    sharedCanvas :null,
    context :null,
    itemPosition :{},
    itemLittlePos :{},
    //初始化
    init:function(){
        this.sharedCanvas =  wx.getSharedCanvas();
        this.context = this.sharedCanvas.getContext('2d');
        this.context.font = "normal 35px Times New Roman";
        this.context.textAlign = "center";
    },
    //清空共享屏
    clearCanvas:function(){
        this.context.clearRect(0, 0,this.sharedCanvas.width, this.sharedCanvas.height);
    },
    //设置竖向各元素的位置
    setVecPos:function(coor){
        var itemPosition = this.itemPosition;
        if (itemPosition.isSet)
            return;
        //计算上下相邻两个之间的间隔
        itemPosition.icon = coor.icon.toString().split("-");
        itemPosition.iconUser = coor.iconUser.toString().split("-");
        itemPosition.rank = coor.rank.toString().split('-');
        itemPosition.rankUser = coor.rankUser.toString().split('-');
        itemPosition.nickName = coor.nickName.toString().split('-');
        itemPosition.nickNameUser = coor.nickNameUser.toString().split('-');
        itemPosition.scorePos = coor.scorePos.toString().split('-');
        itemPosition.scorePosUser = coor.scorePosUser.toString().split('-');
        itemPosition.itemHeight = coor.itemHeight;
        itemPosition.itemWidth = coor.itemWidth;
        itemPosition.backItem = coor.backItem.toString().split('-');
        itemPosition.isSet = true;
    },
    //设置横向各元素的位置
    setHorPos:function(coor) {
        var itemLittlePos = this.itemLittlePos;
        if (itemLittlePos.isSet)
            return;
        //计算上下相邻两个之间的间隔
        itemLittlePos.icon = coor.icon.toString().split("-");
        itemLittlePos.rank = coor.rank.toString().split('-');
        itemLittlePos.nickName = coor.nickName.toString().split('-');
        itemLittlePos.scorePos = coor.scorePos.toString().split('-');
        itemLittlePos.itemHeight = coor.itemHeight;
        itemLittlePos.itemWidth = coor.itemWidth;
        itemLittlePos.leftItem = coor.leftItem.toString().split('-');
        itemLittlePos.score = coor.score;
        itemLittlePos.isSet = true;
    },
    //绘制竖向排行榜用户
    drawVecItem:function(userData, i,  isUser){
        var self = this;
        if (userData == null)
            return;
        var context = this.context;
        var itemPosition = this.itemPosition;
        var y0 = i * itemPosition.itemHeight;
        var url = userData.avatarUrl + '?aaa=aa.jpg';
        var nickName = userData.nickname;
        var score = userData.KVDataList[0].value;
        var image = wx.createImage();
        image.onload =  ()=>{
            var width = image.width / 2;
            var height = image.height / 2;
            if (isUser) {
                context.fillStyle = 'white';
                context.fillText("" + (userData.index + 1), parseFloat(itemPosition.rankUser[0]), parseFloat(itemPosition.rankUser[1]));
                context.drawImage(image, parseFloat(itemPosition.iconUser[0]), parseFloat(itemPosition.iconUser[1]), width, height);
                context.textAlign = "left";
                context.fillText(nickName, parseFloat(itemPosition.nickNameUser[0]), parseFloat(itemPosition.nickNameUser[1]));
                context.textAlign = "center";
                context.fillText(score, parseFloat(itemPosition.scorePosUser[0]), parseFloat(itemPosition.scorePosUser[1]));
            } else {
                if(i == 0)
                    context.fillStyle = '#333333';
                else
                    context.fillStyle = '#222222';
                self.context.fillRect(parseFloat(itemPosition.backItem[0]), parseFloat(itemPosition.backItem[1]) + y0, itemPosition.itemWidth, itemPosition.itemHeight);
                context.fillStyle = 'white';
                context.fillText("" + (1+userData.index), parseFloat(itemPosition.rank[0]), parseFloat(itemPosition.rank[1]) + y0);
                context.drawImage(image, parseFloat(itemPosition.icon[0]), parseFloat(itemPosition.icon[1]) + y0, width, height);
                context.textAlign = "left";
                context.fillText(nickName, parseFloat(itemPosition.nickName[0]), parseFloat(itemPosition.nickName[1]) + y0);
                context.textAlign = "center";
                context.fillText(score, parseFloat(itemPosition.scorePos[0]), parseFloat(itemPosition.scorePos[1]) + y0);
            }
        };
        image.src = url;
    },
    //绘制横向小排行榜用户
    drawHorItem:function(userData,i){
        if(userData == null)
            return;
        var context = this.context;
        var itemPosition = this.itemLittlePos;
        var x0 = i * itemPosition.itemWidth;
        var url = userData.avatarUrl + '?aaa=aa.jpg';
        var nickName = userData.nickname;
        var score = userData.KVDataList[0].value;
        var image = wx.createImage();
        image.onload =  ()=> {
            var width = image.width / 2;
            var height = image.height / 2;
            var flag = i % 2;
            // if (flag == 0)
            //     context.fillStyle = '#222222';
            // else
            //     context.fillStyle = '#333333';
            // context.textAlign = "left";
            // context.fillRect(parseFloat(itemPosition.leftItem[0]) + x0, parseFloat(itemPosition.leftItem[1]), itemPosition.itemWidth, itemPosition.itemHeight);
            context.textAlign = "center";
            context.fillStyle = 'white';
            context.fillText("" +(userData.index + 1), parseFloat(itemPosition.rank[0]) + x0, parseFloat(itemPosition.rank[1]) );
            context.drawImage(image, parseFloat(itemPosition.icon[0]) + x0, parseFloat(itemPosition.icon[1]), width, height);
            context.fillText(nickName, parseFloat(itemPosition.nickName[0]) + x0, parseFloat(itemPosition.nickName[1]));
            context.fillText(score, parseFloat(itemPosition.scorePos[0]) + x0, parseFloat(itemPosition.scorePos[1]));
        };
        image.src = url;
    },
    drawUser:function(userInfo){
        this.drawVecItem(userInfo,0,true);
    },
    //绘制或者重绘小排行榜
    refreshHor:function(horList){
        for(var j = 0;j<horList.length;j++)
            this.drawHorItem(horList[j],j);
    },
    //绘制竖直排行速榜
    refreshVec:function (vecList) {
        for(var j = 0;j<vecList.length;j++)
            this.drawVecItem(vecList[j],j,false);
    }
};
export default drawHelper;
