/**
 * Created by zhanghua on 2018/5/25.
 */
var flax = flax || {};

//初始化存档
//todo 进一步实现存储最高分，一天挑战次数等功能
var clinetId = 'CrazyHex';
var _cookie = localStorage.getItem(clinetId);
if(_cookie){
    flax.userData = JSON.parse(_cookie);
}else{
    flax.userData = {
        userLevel : 0,          //用户关卡
        userPoint : 0,      //用户总得分
        maxScore:0,         //用户最高得分
        finalMax:0,         //用户无限关卡最高得分
        gameOver:false,     //是否通关
        timeStamp:0,        //当天0点
        times:0             // 当天挑战次数
    };
}

flax.userData.save = function () {
    localStorage.setItem(clinetId,JSON.stringify(flax.userData));
};

flax.userData.get = function(key){
    var _data = localStorage.getItem(clinetId);
    if(!_data)
        return 0;
    _data = JSON.parse(_data);
    return _data[key];
};



flax.game.onStart = function () {
    //初始化引擎
    // var policy = flax.isMobile ? flax.ResolutionPolicy.NO_BORDER : flax.ResolutionPolicy.SHOW_ALL;
    var policy = flax.ResolutionPolicy.SHOW_ALL;
    flax.init(policy, null,{
        // transparent: true,
        backgroundColor: allEnum.color.black,
        clearBeforeRender: true
    });
    // Pass true to enable retina display, disabled by default to improve performance
    flax.view.enableRetina(true);
    registerModules();
    flax.replaceScene("login");
};

initData();

//程序启动
flax.game.run();

/**
 * Add all the modules in your game.
 * Note that the modules in the parent class should be added before the sub class
 * */
//注册场景（参数：场景名字，场景，所需素材）
function registerModules() {
    flax.registerScene("main", scenes.mainScene, resource.res_main);
    flax.registerScene("login", scenes.loginScene, resource.res_login);
}

//这里因为在主域无法获得用户托管数据，所有用户在向卸载小游戏后关卡数会被重置为1，除非用一个服务器保存
function initData() {
    //微信初始化
    // weixinHelper.initData();
    // weixinHelper.initWeiXin();
    //初始化游戏关卡
    levelUtil.initLevelAll();
}
